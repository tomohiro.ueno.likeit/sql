//正解
INSERT INTO item_category(category_id,category_name) VALUES (1,'家具'),(2,'食品'),(3,'本');

//category_idはauto_incrementなので描かなくても良い
INSERT INTO item_category(category_name) VALUES ('家具', '食品','本');

//15行目を分けて書く場合
INSERT INTO item_category(category_id,category_name) VALUES (1,'家具');
INSERT INTO item_category(category_id,category_name) VALUES (2,'食品');
INSERT INTO item_category(category_id,category_name) VALUES (3,'本');

//これはダメな例
INSERT INTO item_category(category_id,category_id,category_id, category_name,category_name,category_name) VALUES(1,2,3,'家具','食品','本');
